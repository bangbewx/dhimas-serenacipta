#import "CXMLDocument.h"

#include <libxml/parser.h>

@interface CXMLDocument (CXMLDocument_PrivateExtensions)
- (NSMutableSet *)nodePool;

@end
