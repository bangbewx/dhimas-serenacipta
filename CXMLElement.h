
#import "CXMLNode.h"

@interface CXMLElement : CXMLNode {

}

- (NSArray *)elementsForName:(NSString *)name;
- (NSArray *)elementsForLocalName:(NSString *)localName URI:(NSString *)URI;

- (NSArray *)attributes;
- (CXMLNode *)attributeForName:(NSString *)name;
- (CXMLNode *)attributeForLocalName:(NSString *)localName URI:(NSString *)URI;

- (NSArray *)namespaces; 
- (CXMLNode *)namespaceForPrefix:(NSString *)name;
- (CXMLNode *)resolveNamespaceForName:(NSString *)name;
- (NSString *)resolvePrefixForNamespaceURI:(NSString *)namespaceURI;

@end
