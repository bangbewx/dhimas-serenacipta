#import "CXMLNode.h"

enum {
	CXMLDocumentTidyHTML = 1 << 9,
	CXMLDocumentTidyXML = 1 << 10,
};

@class CXMLElement;

@interface CXMLDocument : CXMLNode {
	NSMutableSet *nodePool;
}

- (id)initWithData:(NSData *)inData options:(NSUInteger)inOptions error:(NSError **)outError;
- (id)initWithData:(NSData *)inData encoding:(NSStringEncoding)encoding options:(NSUInteger)inOptions error:(NSError **)outError;
- (id)initWithXMLString:(NSString *)inString options:(NSUInteger)inOptions error:(NSError **)outError;
- (id)initWithContentsOfURL:(NSURL *)inURL options:(NSUInteger)inOptions error:(NSError **)outError;
- (id)initWithContentsOfURL:(NSURL *)inURL encoding:(NSStringEncoding)encoding options:(NSUInteger)inOptions error:(NSError **)outError;

- (CXMLElement *)rootElement;

- (NSData *)XMLData;
- (NSData *)XMLDataWithOptions:(NSUInteger)options;

- (id)XMLStringWithOptions:(NSUInteger)options;

@end
