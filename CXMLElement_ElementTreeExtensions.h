#import "CXMLElement.h"


@interface CXMLElement (CXMLElement_ElementTreeExtensions)

- (CXMLElement *)subelement:(NSString *)inName;

@end
