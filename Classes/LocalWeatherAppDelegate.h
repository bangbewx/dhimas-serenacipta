#import <UIKit/UIKit.h>

@class LocalWeatherViewController;

@interface LocalWeatherAppDelegate : NSObject <UIApplicationDelegate> {
    UIWindow *window;
    LocalWeatherViewController *viewController;
}

@property (nonatomic, retain) IBOutlet UIWindow *window;
@property (nonatomic, retain) IBOutlet LocalWeatherViewController *viewController;

@end

