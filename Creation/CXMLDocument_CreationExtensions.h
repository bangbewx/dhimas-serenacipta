import "CXMLDocument.h"

@interface CXMLDocument (CXMLDocument_CreationExtensions)

- (void)insertChild:(CXMLNode *)child atIndex:(NSUInteger)index;

- (void)addChild:(CXMLNode *)child;

@end
