#import "CXMLNode.h"

@class CXMLElement;

@interface CXMLNode (CXMLNode_CreationExtensions)

+ (id)document;
+ (id)documentWithRootElement:(CXMLElement *)element;
+ (id)elementWithName:(NSString *)name;
+ (id)elementWithName:(NSString *)name URI:(NSString *)URI;
+ (id)elementWithName:(NSString *)name stringValue:(NSString *)string;
+ (id)namespaceWithName:(NSString *)name stringValue:(NSString *)stringValue;
+ (id)processingInstructionWithName:(NSString *)name stringValue:(NSString *)stringValue;

- (void)setStringValue:(NSString *)inStringValue;

@end
